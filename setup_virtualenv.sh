#!/bin/bash

set -e

if [[ -d /var/local/pip_download_cache ]]; then
    export PIP_DOWNLOAD_CACHE=/var/local/pip_download_cache
else
    export PIP_DOWNLOAD_CACHE=$HOME/.pip_download_cache
fi

appdir=$(pwd)
application=$(basename "$appdir")
cfgdir="$appdir/config"

venv='venv'
virtualenv='virtualenv'
pythonRepo='https://pypi.python.org/simple' # Using std python repo
knewtonPythonRepo='https://pypi.knewton.net/simple'

echo "Setting up virtualenv for $application in $appdir/$venv"
if [ -d "$appdir/$venv" ]; then
    echo "Clearing existing virtualenv..."
    rm -rf $appdir/$venv
fi

mkdir -p "$appdir/$venv"

$virtualenv $venv
source $venv/bin/activate

# Update setuptools if necessary
pip install setuptools --upgrade --index-url $pythonRepo

# Install ipython by default
pip install ipython --index-url $pythonRepo

# Install all requirements
pip install --requirement "$cfgdir/requirements.all.txt" --index-url $pythonRepo

# If necessary, install dependent requirements in second wave
# For e.g. due to bug in numpy and matplotlib
if [ -e "$cfgdir/requirements.second.txt" ]; then
    pip install --requirement "$cfgdir/requirements.second.txt" --index-url $pythonRepo
fi

# If necessary, install knewton requirements also
if [ -e "$cfgdir/requirements.knewton.txt" ]; then
    pip install --requirement "$cfgdir/requirements.knewton.txt" --index-url $knewtonPythonRepo
fi
